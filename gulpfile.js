var gulp = require('gulp');
var livereload = require('gulp-livereload')
var uglify = require('gulp-uglifyjs');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');




gulp.task('imagemin', function () {
    return gulp.src('./wp-content/themes/intermedia/images/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('./wp-content/themes/intermedia/images'));
});


gulp.task('sass', function () {
  gulp.src('./wp-content/themes/intermedia/sass/**/*.scss')
    .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 7', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./wp-content/themes/intermedia'));
});


gulp.task('uglify', function() {
  gulp.src('./wp-content/themes/intermedia/lib/*.js')
    .pipe(uglify('intermedia.min.js'))
    .pipe(gulp.dest('./wp-content/themes/intermedia/js'))
});

gulp.task('watch', function(){
    livereload.listen();

    gulp.watch('./wp-content/themes/intermedia/sass/**/*.scss', ['sass']);
    gulp.watch('./wp-content/themes/intermedia/lib/*.js', ['uglify']);
    gulp.watch(['./wp-content/themes/intermedia/style.css', './wp-content/themes/intermedia/*.php', './wp-content/themes/intermedia/js/*.js', './wp-content/themes/intermedia/parts/**/*.php'], function (files){
        livereload.changed(files)
    });
});
 
