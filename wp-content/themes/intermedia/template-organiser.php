<?php
/*
Template name: Template Organiser
*/
?>
<?php get_header(); ?>
<div id="organiser" class="container">
    <div class="entry-header">
        <h1 class="entry-title">
			<?php echo get_the_title(); ?>
        </h1>
    </div>
    <div class="row">
<?php
$loop = new WP_Query( array( 'post_type' => 'organiser','sort' => 'AEC',  'ignore_sticky_posts' => 1, 'paged' => $paged ) );
if ( $loop->have_posts() ) :
	while ( $loop->have_posts() ) : $loop->the_post(); ?>
		<div class="col-md-6">
			<div class="pindex">
				<div class="pcontent">
					<h5><?php echo get_the_title(); ?></h5>
					<?php if ( has_post_thumbnail() ) { ?>
                        <div class="pimagel">
							<?php the_post_thumbnail(); ?>
                        </div>
					<?php } ?>
					<p><?php echo get_the_content(); ?></p>

				</div>
			</div>
		</div>
	<?php endwhile;
endif;
wp_reset_postdata();
?>
</div>
</div>

<?php get_footer(); ?>
