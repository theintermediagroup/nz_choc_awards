<?php
/*
* Template Name: Login 
*/
require_once(dirname(__FILE__) . '/Lib.php');
if(!empty($_POST)) {
        $user = login(array('user_login'=>$_POST['username'], 'user_password'=>$_POST['password'], 'remember'=>false));
        if($user !== false) {
                header('Location: /dashboard/');
        }
}
if(is_user_logged_in()) {
        header('location: /dashboard/');
}

get_header();
?>
<script type="text/javascript">
        jQuery(document).ready(function(){
        var edm = new Edm(); 
                jQuery('#login_form').validate({
                        rules: {
                                username: {required: true}, 
                                password: {required: true},
                        }, 
                        messages: {
                                username: "Username is required", 
                                password: "Password is required",
                        },
                        invalidHandler: function(event, validator){
                edm.hideScreenCover(); 
                }, 
                        submitHandler: function(form) {
                                edm.showScreenCover();
                                form.submit(); 
                        }
                });
        }); 
        
</script>

<form name="login_form" id="login_form" method="post">
<table id="loginArea" style="max-width: 700px;margin: 20px auto;">
        <tr><td colspan="2" style="text-align:center;"><h1 class="site-title"><?php bloginfo( 'name' ); ?></h1></td></tr>
        <tr><td colspan="2">&nbsp;</td></tr>
        <tr><th>Username<span class="required">*</span></th><td><input type="text" name="username" id="username"/></td></tr>
        <tr><th>Password<span class="required">*</span></th><td><input type="password" name="password" id="password"/></td></tr>
<?php
        if(!empty($_POST)) {
                $user = login(array('user_login'=>$_POST['username'], 'user_password'=>$_POST['password'], 'remember'=>false));
                if($user !== false) {
                        header('Location: /dashboard/');
                }
        }
?>
    <tr>
        <td></td><td style="text-align:right;"><input type="submit" id="login" name="login" value="Login" class="btn"/></td>
    </tr>
</table>
</form>
<?php
get_footer();
?>

