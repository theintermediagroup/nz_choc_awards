<?php get_header(); ?>
<?php
/**
 * @package AmitBaral
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="container">
	<div class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</div><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>

	</div><!-- .entry-content -->
</div>

</article><!-- #post-## -->

<?php get_footer(); ?>
