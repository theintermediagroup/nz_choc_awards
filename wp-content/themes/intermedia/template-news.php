<?php
/*
Template name: Template News
*/
?>
<?php get_header(); ?>
<div id="judges" class="container">
	<div class="entry-header">
		<h1 class="entry-title">
			<?php echo get_the_title(); ?>
		</h1>
		<p><?php echo get_the_content(); ?></p>
	</div>
	<?php
	$loop = new WP_Query( array( 'post_type' => 'post',  'ignore_sticky_posts' => 1, 'paged' => $paged ) );
	if ( $loop->have_posts() ) :
		while ( $loop->have_posts() ) : $loop->the_post(); ?>
			<div class="row">
				<div class="col-md-4">
					<div class="pindex">
						<?php if ( has_post_thumbnail() ) { ?>
							<div class="pimage">
								<a href="<?php echo get_the_permalink(); ?>">
									<?php the_post_thumbnail('blog-image-size', ['class' => 'img-responsive responsive--full']); ?>
								</a>
							</div>
						<?php } ?>
						<div class="pcontent">
							<h5><a href="<?php echo get_the_permalink(); ?>"> <?php echo get_the_title(); ?> </a> </h5>
							<p><?php echo get_the_excerpt(); ?></p>

						</div>
					</div>
				</div>
			</div>

		<?php endwhile;
	endif;
	wp_reset_postdata();
	?>
</div>

<?php get_footer(); ?>
