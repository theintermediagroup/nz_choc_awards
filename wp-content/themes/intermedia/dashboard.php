<?php 
/*    
*Template Name: Dashboard 
*/
if(!is_user_logged_in()) {
        header('location: /login/');
        exit;           
}                       
                          
get_header();             
                                
$current_user = wp_get_current_user();
if(count($current_user->roles) > 1) {
                $user_role = $current_user->roles[1];
        }else { 
                $user_role = $current_user->roles[0];
        }       
                        
    $user_id = $current_user->data->ID; 
?>
<?php get_footer(); ?>

