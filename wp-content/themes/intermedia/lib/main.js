jQuery(document).ready(function($) {
    $('#clock').countdown('2017/09/06', function(event) {
        var $this = $(this).html(event.strftime(''
            + '<ul class="timmer">'
            + '<li><div class="number">%w</div><span>weeks</span></li>'
            + '<li><div class="number">%d</div><span>days</span></li>'
            + '<li><div class="number">%H</div><span>hr</span></li>'
            + '<li><div class="number">%M</div><span>min</span></li>'
            + '<li><div class="number">%S</div><span>sec</span></li>'
            + '</ul>'));
    });
});
