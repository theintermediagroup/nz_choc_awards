<?php

/*  Register Scripts and Style */
require_once('wp-bootstrap-navwalker.php');

function theme_register_scripts() {
	wp_enqueue_style( 'bootstrap-css', get_stylesheet_directory_uri() . '/vendor/twbs/bootstrap/dist/css/bootstrap.min.css');
    wp_enqueue_style( 'intermedia-css', get_stylesheet_uri() );
    wp_enqueue_script( 'intermedia-js', esc_url( trailingslashit( get_template_directory_uri() ) . 'js/intermedia.min.js' ), array( 'jquery' ), '1.0', true );
    wp_enqueue_script('bootstrap-script', get_stylesheet_directory_uri() .'/vendor/twbs/bootstrap/dist/js/bootstrap.min.js',array('jquery'),'324', true);
    wp_enqueue_script('countdown-script', get_stylesheet_directory_uri() .'/bower_components/jquery.countdown/dist/jquery.countdown.js',array('jquery'),'324', true);

}
add_action( 'wp_enqueue_scripts', 'theme_register_scripts', 1 );

add_theme_support( 'custom-logo' );
add_post_type_support( 'page', 'excerpt' );

/* Add menu support */
if (function_exists('add_theme_support')) {
    add_theme_support('menus');
}



function organisers_post_types() {
	$labels = array(
		'name'               => 'Organisers',
		'singular_name'      => 'Organiser',
		'menu_name'          => 'Organiser',
		'name_admin_bar'     => 'Organisers',
		'add_new'            => 'Add New',
		'add_new_item'       => 'Add New Organiser',
		'new_item'           => 'New Organiser',
		'edit_item'          => 'Edit Organiser',
		'view_item'          => 'View Organiser',
		'all_items'          => 'All Organisers',
		'search_items'       => 'Search Organisers',
		'parent_item_colon'  => 'Parent Organisers:',
		'not_found'          => 'No organiser found.',
		'not_found_in_trash' => 'No organiser found in Trash.'
	);

	$args = array(
		'public'      => true,
		'labels'      => $labels,
		'description' => 'Award Organisers',
		'supports' => ['title', 'editor', 'thumbnail']
	);
	register_post_type( 'organiser', $args );
}
add_action( 'init', 'organisers_post_types' );

function judges_post_types() {
	$labels = array(
		'name'               => 'Judges',
		'singular_name'      => 'Judge',
		'menu_name'          => 'Judge',
		'name_admin_bar'     => 'Judges',
		'add_new'            => 'Add New',
		'add_new_item'       => 'Add New Judge',
		'new_item'           => 'New Judge',
		'edit_item'          => 'Edit Judge',
		'view_item'          => 'View Judge',
		'all_items'          => 'All Judges',
		'search_items'       => 'Search Judges',
		'parent_item_colon'  => 'Parent Judges:',
		'not_found'          => 'No judge found.',
		'not_found_in_trash' => 'No judge found in Trash.'
	);

	$args = array(
		'public'      => true,
		'labels'      => $labels,
		'description' => 'Award Judges',
		'supports' => ['title', 'editor', 'thumbnail'],
	);
	register_post_type( 'judges', $args );
}
add_action( 'init', 'judges_post_types' );





function sponsors_post_types() {
	$labels = array(
		'name'               => 'Sponsors',
		'singular_name'      => 'Sponsor',
		'menu_name'          => 'Sponsor',
		'name_admin_bar'     => 'Sponsors',
		'add_new'            => 'Add New',
		'add_new_item'       => 'Add New Sponsor',
		'new_item'           => 'New Sponsor',
		'edit_item'          => 'Edit Sponsor',
		'view_item'          => 'View Sponsor',
		'all_items'          => 'All Sponsors',
		'search_items'       => 'Search Sponsors',
		'parent_item_colon'  => 'Parent Sponsor:',
		'not_found'          => 'No sponsor found.',
		'not_found_in_trash' => 'No sponsor found in Trash.'
	);

	$args = array(
		'public'      => true,
		'labels'      => $labels,
		'description' => 'Award Sponsors',
		'supports' => ['title', 'editor', 'thumbnail'],
	);
	register_post_type( 'sponsors', $args );
}
add_action( 'init', 'sponsors_post_types' );




/* Add post image support */
add_theme_support( 'post-thumbnails' );



/* Add custom thumbnail sizes */
if ( function_exists( 'add_image_size' ) ) {
    add_image_size( 'blog-image-size', 660, 394, true );
	add_image_size( 'large-blog-image-size', 730, 487, true );
}

/* Add widget support */
if ( function_exists('register_sidebar') )
    register_sidebar(array(
        'name'          => 'FooterOne',
        'id'            => 'FooterOne',
	    'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widgettitle">',
        'after_title'   => '</h4>',
    ));

if ( function_exists('register_sidebar') )
    register_sidebar(array(
        'name'          => 'FooterTwo',
        'id'            => 'FooterTwo',
	    'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widgettitle">',
        'after_title'   => '</h4>',
    ));
if ( function_exists('register_sidebar') )
	register_sidebar(array(
		'name'          => 'FooterThree',
		'id'            => 'FooterThree',
		'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="widgettitle">',
		'after_title'   => '</h4>',
	));

register_nav_menus( array(
	'primary' => __( 'Primary Menu', 'THEMENAME' ),
) );


/*  EXCERPT
    Usage:

    <?php echo excerpt(100); ?>
*/

function excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
    } else {
    $excerpt = implode(" ",$excerpt);
    }
    $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
    return $excerpt;
}
