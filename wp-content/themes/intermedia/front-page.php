<?php get_header(); ?>
<section id="about">
    <div class="container wrapper">
        <div class="row">
            <div class="col-md-6">
                <div class="pad-wrap">
	                <?php
	                // query for the about page
	                $your_query = new WP_Query( 'pagename=about' );
	                // "loop" through query (even though it's just one page)
	                while ( $your_query->have_posts() ) : $your_query->the_post();
	                echo  the_title('<h3>', '</h3>') ;
	                 echo excerpt(1000);
	                endwhile;
	                // reset post data (important!)
	                wp_reset_postdata();
	                ?>
<div class="readmore">
    <a href="/about/">
        READ MORE
    </a>
</div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="no-pad-wrap">
                    <img src="<?php echo get_bloginfo('template_directory') ?>/images/cup.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<section id="timeline" class="container">
	<div class="">
		<div class="col-md-6">
			<div class="history-tl-container">
                <div class="timeline">
                    <span>TIMELINE</span>
                </div>
				<ul class="tl">
					<li class="tl-item" ng-repeat="item in retailer_history">
						<div class="timestamp">
							MON 17 Jul 2017
						</div>
						<div class="item-title">Entries open</div>
					</li>
					<li class="tl-item" ng-repeat="item in retailer_history">
						<div class="timestamp">
							MON 23 Aug 2017
						</div>
						<div class="item-title">Entries close</div>
					</li>
					<li class="tl-item" ng-repeat="item in retailer_history">
						<div class="timestamp">
							TUE 5 Sept 2017
						</div>
						<div class="item-title">Final day for judging samples to arrive in Auckland</div>
					</li>
					<li class="tl-item" ng-repeat="item in retailer_history">
						<div class="timestamp">
							WED 6 Sept 2017
						</div>
						<div class="item-title">Judging, Auckland</div>
					</li>
					<li class="tl-item" ng-repeat="item in retailer_history">
						<div class="timestamp">
							MON 18 Sept 2017
						</div>
						<div class="item-title">Finalists and winners announced</div>
					</li>
					<li class="tl-item" ng-repeat="item in retailer_history">
						<div class="timestamp">
							SAT 23 and SUN 24 Sept 2017
						</div>
						<div class="item-title">Winning products tasted at
							The Chocolate and Coffee Show </div>
					</li>
				</ul>
			</div>
		</div>
		<div class="col-md-6">
			<div class="coffshow">
				<?php
				// query for the about page
				$your_query = new WP_Query( 'pagename=the-chocolate-and-coffee-show' );
				// "loop" through query (even though it's just one page)
				while ( $your_query->have_posts() ) : $your_query->the_post();
					echo  the_title('<h3 style="margin-bottom: 30px;">', '</h3>') ;
					echo the_content();
				endwhile;
				// reset post data (important!)
				wp_reset_postdata();
				?>
            </div>
		</div>
	</div>
</section>
<section id="news" >
	<div class="container">
        <div class="row">
			<?php
			$args = array(
				'post_type' => 'post',
				'posts_per_page' => 3
			);

			$post_query = new WP_Query($args);
			if($post_query->have_posts() ) {
				while($post_query->have_posts() ) {
					$post_query->the_post();
					?>
                    <div class="col-md-4">
                        <div class="sm-wrap">
							<?php
							if ( has_post_thumbnail( $post->ID ) ) {
								echo '<a href="' . get_permalink( $post->ID ) . '" title="' . esc_attr( $post->post_title ) . '">';
								echo get_the_post_thumbnail( $post->ID, 'blog-image-size' );
								echo '</a>';
							}
							?>
                            <h5><?php the_title(); ?></h5>
                            <?php the_excerpt(); ?>
                        </div>
                    </div>
					<?php
				}
			}
			?>

        </div>
        <div class="viewall"><a href="/news/">VIEW ALL NEWS</a></div>
    </div>

</section>
<!--    <section id="judge">-->
<!--        <div class="container wrapper">-->
<!--            <div class="row">-->
<!--                <div class="col-md-6">-->
<!--                    <div class="no-pad-wrap">-->
<!--                        <img src="--><?php //echo get_bloginfo('template_directory') ?><!--/images/judge.png" alt="">-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-md-6">-->
<!--                    <div class="pad-wrap">-->
<!--                        <h3>Judging Process</h3>-->
<!--                        <p>When creating the NZ Chocolate Awards it was important to us to develop a judging system that was fair, transparent and easy to use. The awards will be judged in a way that is well thought out and credible.</p>-->
<!--                        <div class="readmore">-->
<!--                            <a href="/judging-process/">-->
<!--                                READ MORE-->
<!--                            </a>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </section>-->


<?php get_footer(); ?>
