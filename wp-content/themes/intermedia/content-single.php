<?php
/**
 * @package AmitBaral
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content great<?php the_ID(); ?>">
		<div class="container">
            <div class="col-md-8 col-md-offset-2">
			<div class="entry-header" style="margin-top: 50px;">

				<?php if ( has_post_thumbnail() ) { ?>
                    <div class="pimage">
						<?php the_post_thumbnail('large-blog-image-size', ['class' => 'img-responsive responsive--full']); ?>
                    </div>
				<?php } ?>
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			</div><!-- .entry-header -->
	            <?php the_content(); ?>
            </div>

		</div>
	</div><!-- .entry-content -->


</article><!-- #post-## -->
