<?php
/*
Template name: Template About
*/
?>
<?php get_header(); ?>
<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
<div class="container">
		<?php while ( have_posts() ) : the_post(); ?>
        <div class="entry-header">
            <h1 class="entry-title">
			    <?php echo get_the_title(); ?>
            </h1>
        </div>
            <div class="row">
                <div class="desktop col-md-6">
	                <?php if ( has_post_thumbnail() ) { ?>
                        <div class="pimage">
			                <?php the_post_thumbnail(); ?>
                        </div>
	                <?php } ?>
                </div>
                <div class="col-md-6">
                    <div class="section-message-left">
                        <div class="pcontent">
                            <p><?php echo get_the_content(); ?></p>
                        </div>

                    </div>
                </div>

		<?php endwhile; // end of the loop. ?>
            </div>
    </main><!-- #main -->
</div><!-- #primary -->
<?php get_footer(); ?>
