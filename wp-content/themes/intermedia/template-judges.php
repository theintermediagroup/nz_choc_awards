<?php
/*
Template name: Template Judges
*/
?>
<?php get_header(); ?>
<div id="judges" class="container">
	<div class="entry-header">
		<h1 class="entry-title">
			<?php echo get_the_title(); ?>
		</h1>
        <p><?php echo get_the_content(); ?></p>
	</div>
<?php
$loop = new WP_Query( array( 'post_type' => 'judges',  'ignore_sticky_posts' => 1, 'paged' => $paged ) );
if ( $loop->have_posts() ) :
	while ( $loop->have_posts() ) : $loop->the_post(); ?>
        <div class="row">
            <div class="col-md-3">
                <div class="pindex">
			        <?php if ( has_post_thumbnail() ) { ?>
                        <div class="pimage">
					        <?php the_post_thumbnail(); ?>
                        </div>
			        <?php } ?>
                </div>
            </div>
            <div class="col-md-9">
                <div class="pindex">
                    <div class="pcontent">
                        <h4><?php echo get_the_title(); ?></h4>
                        <p><?php echo get_the_content(); ?></p>

                    </div>
                </div>
            </div>

        </div>

	<?php endwhile;
endif;
wp_reset_postdata();
?>
</div>

<?php get_footer(); ?>
