
<section id="footer">
<!--    <div class="container wrapper">-->
<!--        <div class="row">-->
<!--            <div class="col-md-4">-->
<!--                <ul id="footerone">-->
<!--					--><?php //dynamic_sidebar( 'FooterOne' ); ?>
<!--                </ul>-->
<!--            </div>-->
<!--            <div class="col-md-4">-->
<!--                <ul id="footertwo">-->
<!--					--><?php //dynamic_sidebar( 'FooterTwo' ); ?>
<!--                </ul>-->
<!--            </div>-->
<!--            <div class="col-md-4">-->
<!--                <ul id="footerthree">-->
<!--					--><?php //dynamic_sidebar( 'FooterThree' ); ?>
<!--                </ul>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
    <div class="custom-menu">
	    <?php wp_nav_menu( array( 'theme_location' => 'footer', 'container_class' => 'new_menu_class' ) ); ?>
    </div>
</section>

<div class="copy">© <a href="http://www.intermedia.com.au/" target="_blank">The Intermedia Group</a></div>
	<?php wp_footer(); ?>
</body>
</html>
