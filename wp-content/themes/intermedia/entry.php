<?php
/*
Template Name: Voting Entry
*/
get_header(); ?>
	<div id="main">
		<div id="content" class="narrowcolumn">
            <section id="entry">
                <div class="container wrapper">
                    <div class="row">
                        <div class="lado img-entry col-md-6">
                            <div class="no-pad-wrap">
                                <img src="<?php echo get_bloginfo('template_directory') ?>/images/entry-form.png" alt="">
                            </div>
                        </div>
                        <div class="puti content-entry col-md-6">
                            <div class="pad-wrap">
                                <h4>NZ CHOCOLATE AWARDS ENTRY FORM</h4>
                                <p>The simple, easy way to enter is via the online form below.</p>

                                <p>When filling out the form complete an answer in ALL boxes. If the question is not relevant write ‘not applicable’.</p>

                                <p>Entry is $75+GST or $86.25 per product. If you are entering more than five products contact us for a discount code.
                                <p>Kathie Bartley <a href="mailto:kathie@marvellousmarketing.co.nz">Kathie@marvellousmarketing.co.nz </a> or</p>
                                   <p> Nicola McConnell <a href="mailto:nicola@marvellousmarketing.co.nz"> Nicola@marvellousmarketing.co.nz</a></p>

                                <p>Entrants will receive a tax invoice via email.</p>

                                <p>If you have any issues with the entry contact us for a paper Entry Form.</p>

                                <div class="readmore">
                                    <a href="/fine-print/">
                                        READ THE FINE PRINT HERE
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<div id="post-<?php the_ID(); ?>">
					<h1><?php// the_title(); ?></h1>
					<div class="container">
						<?php the_content(); ?>

					</div>
					<?php endwhile; endif; ?>
	</div>
            <script>
              jQuery( document ).ready(function() {
                jQuery(".puti").css({'height':(jQuery(".lado").height()+'px')});
              });

            </script>

<?php get_footer(); ?>
