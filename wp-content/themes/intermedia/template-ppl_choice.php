<?php
/*
Template Name: Template People Choice
*/
get_header(); ?>
	<div id="main">
		<div id="content" class="narrowcolumn">
            <section id="entry">
                <div class="container wrapper">
                    <div class="row">
                        <div style="background: url(<?php echo get_bloginfo('template_directory') ?>/images/chocolate.png)" class="lado img-entry col-md-6">
                            <div class="no-pad-wrap">

                            </div>
                        </div>
                        <div class="puti content-entry col-md-6">
                            <div class="pad-wrap">
	                            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                                <div id="post-<?php the_ID(); ?>">
                                    <h1><?php// the_title(); ?></h1>
                                    <div class="hero-cont">
			                            <?php the_content(); ?>

                                    </div>
		                            <?php endwhile; endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
	                        <?php echo do_shortcode('[gravityform id="3" title="true" description="true" ajax="true"]') ?>
                        </div>
                    </div>
                </div>
            </section>

            <script>
              jQuery( document ).ready(function() {
                jQuery(".lado").css({'height':(jQuery(".puti").height()+'px')});
              });

            </script>

<?php get_footer(); ?>
